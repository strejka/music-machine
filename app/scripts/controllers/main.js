'use strict';


/**
 * @ngdoc function
 * @name musicMachineApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the musicMachineApp
 */
var mm = angular.module('musicMachineApp');
mm.controller('MainCtrl', ['$scope', '$location', 'timerFactory', function ($scope, $location, timer) {

  var self = this;
  this.maintitle = 'music machine';
  this.headline = '';
  this.description = '';
  this.shareLink = $location.absUrl();
  this.playing = false;
  this.length = 8;
  this.tempo = 240; //in BPM
  this.interval = 60000 / this.tempo; //in ms
  // this.timer = null;
  this.i = 0; //index of current beat
  this.play = function(){

    this.playing = true;
    // this.timer = new interval(this.interval, this.step);
    // this.timer.run();
    timer.start(this.interval, this.step);
    this.playing = true;
  };
  this.step = function(){
    angular.forEach(self.instruments, function(instrument){
      if(instrument.beats[self.i]){
        instrument.play();
      }
    });
    $scope.$apply();
    self.i = ((self.i+1) !== self.length)?self.i+1:0;
    // $('h1').css('text-shadow', Math.random()*30+'px '+Math.random()*10+'px '+Math.random()*8+'px #fff');

  };
  this.stop = function(){
    timer.stop();
    this.playing = false;
    self.i = 0;
  };
  this.pause = function(){
    if(this.playing){
      this.timer.stop();
      this.playing = false;
    }else{
      this.play();
    }
    // clearInterval(this.timer);
  };
  this.tempoChanged = function(){
    this.interval = 60000 / this.tempo;
    if(this.playing){
      this.stop();
      this.play();
    }
  };
  this.serialize = function(){
    var instruments = '';
    angular.forEach(this.instruments, function(i){
      console.log(i.beats);
    });
    return {
      headline : this.headline,
      description : this.description,
      length : this.length,
      tempo : this.tempo
    };
  };
  this.save = function(){
    $location.search(this.serialize());
    this.shareLink = $location.absUrl();
    setTimeout(function(){
      $("#share_url").select();
    },400);
  };
  this.load = function(){
    var o = $location.search();
    this.headline = o.headline;
    this.description = o.description;
    this.length = o.langht || 8;
    this.tempo = o.tempo || 240;
  };

  this.addInstrument = function() {
    var beatLinstKeys = Object.keys(self.beatList);
    var beatKey = beatLinstKeys[this.instruments.length % beatLinstKeys.length];
    var beat = self.beatList[beatKey];
    var instrument = {
      name : beatKey,
      volume : 0.5,
      speed : 1,
      beats : [],
      beat : beatKey,
      audio : null,
      click: function(key){
        this.beats[key] = !this.beats[key];
      },
      play : function(){
        if(this.audio === null)
          this.audio = new Audio(self.beatList[this.beat]);
        this.audio.playbackRate = this.speed;
        this.audio.volume = this.volume;
        this.audio.play();
        this.audio = new Audio(self.beatList[this.beat]);
      },
      setVolume : function(volume){
        this.audio.volume = volume;
      },
      setSpeed : function(speed){
        this.audio.playbackRate = speed;
      },
      delete : function(){
        var index = self.instruments.indexOf(this);
        $("#set-modal-"+index).modal('hide');
        if(index > -1){
          self.instruments.splice(index,1);
        }
      }
    };
    instrument.audio = new Audio(self.beatList[instrument.beat]);
    instrument.beats.length = this.length;
    this.instruments.push(instrument);
  };

  this.instruments = [];
  this.beatList = {
    'kopák'     : 'mp3/k4.mp3',
    'rytmičák'  : 'mp3/k2.mp3',
    'činel2'    : 'mp3/k3.mp3',
    'činel1'    : 'mp3/k1.mp3',
  };

  this.addInstrument();
  this.addInstrument();
  this.addInstrument();

  for(var i = 0; i < this.instruments.length; i++){
    this.instruments[i].beats.length = this.length;
  }
  this.lenghtChange = function(){
    for(var i = 0; i < this.instruments.length; i++){
      this.instruments[i].beats.length = this.length;
    }
  };
  this.load();
}]);



mm.factory('timerFactory', function(){
  window.URL = window.URL || window.webkitURL;
  var workerCode = `
  self.onmessage=function(e){
    data = JSON.parse(e.data);
    console.log(data);
    if(data.action === 'start'){
      self.interval = setInterval(function(){
        postMessage('Worker: '+e.data);
      }, data.tempo);
    }else if(data.action === 'stop'){
      clearInterval(self.interval);
    }
  }`;
  var self = this;
  var blob;
  try {
      blob = new Blob([workerCode], {type: 'application/javascript'});
  } catch (e) {
      window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder;
      blob = new BlobBuilder();
      blob.append(workerCode);
      blob = blob.getBlob();
  }
  var worker = new Worker(URL.createObjectURL(blob));
  worker.onmessage = function(){
    console.log("pow");
    self.step();
  };
  return {
    start : function(tempo, stepFunc){
      self.step = stepFunc;
      worker.postMessage(JSON.stringify({
        action : "start",
        tempo : tempo
      }));
    },
    stop : function(){
      worker.postMessage(JSON.stringify({
        action : "stop"
      }));
    }
  };
});

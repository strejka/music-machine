'use strict';

/**
 * @ngdoc overview
 * @name musicMachineApp
 * @description
 * # musicMachineApp
 *
 * Main module of the application.
 */
angular
  .module('musicMachineApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
